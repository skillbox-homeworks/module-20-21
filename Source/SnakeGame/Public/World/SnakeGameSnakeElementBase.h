// SnakeGame, Copyright FaNtic. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeGameSnakeElementBase.generated.h"

class UStaticMeshComponent;

UCLASS()
class SNAKEGAME_API ASnakeGameSnakeElementBase : public AActor
{
	GENERATED_BODY()

public:

	ASnakeGameSnakeElementBase();

public:

	void UpdateColor(const FLinearColor& Color);

	void UpdateScale(uint32 CellSize);

protected:

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Origin;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* ElementStaticMeshComponent;
};

// SnakeGame, Copyright FaNtic. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Core/Types.h"
#include "World/SnakeGameBiomeColors.h"
#include "SnakeGameGrid.generated.h"

namespace SnakeGame
{
	class Grid;
}

class UStaticMeshComponent;

UCLASS()
class SNAKEGAME_API ASnakeGameGrid : public AActor
{
	GENERATED_BODY()

public:

	ASnakeGameGrid();

protected:

	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Origin;

	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent* GridStaticMesh;

public:

	virtual void Tick(float DeltaTime) override;

	void SetGridModel(const TSharedPtr<SnakeGame::Grid>& Grid, uint32 InitialCellSize);

	void UpdateGridColors(const FBiomeColors& Colors);

private:

	UPROPERTY()
		UMaterialInstanceDynamic* GridMaterial;

	SnakeGame::Dimension GridDimension;
	uint32 CellSize;
	uint32 WorldWidth;
	uint32 WorldHeight;

	void DrawGrid();

};

// SnakeGame, Copyright FaNtic. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Core/Snake.h"
#include "World/SnakeGameSnakeColors.h"
#include "SnakeGameSnake.generated.h"

class ASnakeGameSnakeElementBase;

UCLASS()
class SNAKEGAME_API ASnakeGameSnake : public AActor
{
	GENERATED_BODY()

public:

	ASnakeGameSnake();

protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

public:

	void SetModel(const TSharedPtr<SnakeGame::Snake>& InitialSnake, uint32 InitialCellSize, const SnakeGame::Dimension& InitialDimension);

	void UpdateSnakeColors(const FSnakeColors& Colors);

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<AActor> SnakeHeadClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<AActor> SnakeBodyClass;

private:

	TWeakPtr<SnakeGame::Snake> Snake;
	uint32 CellSize;
	SnakeGame::Dimension Dimension;

	UPROPERTY()
		TArray<ASnakeGameSnakeElementBase*> SnakeNodes;
};

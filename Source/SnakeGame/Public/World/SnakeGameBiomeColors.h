// SnakeGame, Copyright FaNtic. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "SnakeGameBiomeColors.generated.h"

USTRUCT(BlueprintType)
struct FBiomeColors : public FTableRowBase
{
	GENERATED_BODY()

		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FLinearColor BackgroundColor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FLinearColor WallColor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FLinearColor LineColor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FLinearColor SkyAtmosphereColor;
};

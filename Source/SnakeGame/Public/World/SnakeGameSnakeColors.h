// SnakeGame, Copyright FaNtic. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"

#include "SnakeGameSnakeColors.generated.h"

USTRUCT(BlueprintType)
struct FSnakeColors : public FTableRowBase
{
	GENERATED_BODY()

		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FLinearColor HeadColor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FLinearColor BodyColor;
};

// SnakeGame, Copyright FaNtic. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Core/Game.h"
#include "Engine/DataTable.h"
#include "EnhancedInput/Public/InputActionValue.h"
#include "EnhancedInput/Public/InputMappingContext.h"
#include "SnakeGameGameMode.generated.h"


/**
 *
 */
class ASnakeGameGrid;
class ASnakeGameSnake;
class UInputAction;
class UInputMappingContext;
class AExponentialHeightFog;

UCLASS()
class SNAKEGAME_API ASnakeGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	ASnakeGameGameMode();

public:

	virtual void StartPlay() override;

	virtual void Tick(float DeltaSeconds) override;

protected:

	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "10", ClampMax = "100"), Category = "Grid Settings")
		FIntPoint GridSize {
		10, 10
	};

	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "10", ClampMax = "100"), Category = "Grid Settings")
		uint32 CellSize {
		10
	};

	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "4", ClampMax = "8"), Category = "Snake Settings")
		uint32 StartSnakeSize {
		4
	};

	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "0.01", ClampMax = "10"), Category = "Snake Settings")
		float SnakeSpeed{
		1.0f
	};

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeGameGrid> GridVisualClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeGameSnake> SnakeVisualClass;

	UPROPERTY(EditDefaultsOnly, Category = "Biome Colors")
		UDataTable* BiomeColorsTable;

	UPROPERTY(EditDefaultsOnly, Category = "Snake Colors")
		UDataTable* SnakeColorsTable;

protected:

	UPROPERTY(EditDefaultsOnly, Category = "Enhanced Input Actions|Input Actions")
		TSoftObjectPtr<UInputAction> MoveForwardInputAction;

	UPROPERTY(EditDefaultsOnly, Category = "Enhanced Input Actions|Input Actions")
		TSoftObjectPtr<UInputAction> MoveRightInputAction;

	UPROPERTY(EditDefaultsOnly, Category = "Enhanced Input Actions|Input Mapping Contexts")
		TSoftObjectPtr<UInputMappingContext> MovementInputMapping;

private:

	void SetupInput();
	void OnMoveForward(const FInputActionValue& Value);
	void OnMoveRight(const FInputActionValue& Value);

private:

	TUniquePtr<SnakeGame::Game> CoreGame;

	SnakeGame::Input SnakeInput{ 1, 0 };

	UPROPERTY()
		ASnakeGameGrid* GridVisual;

	uint32 BiomeColorTableIndex{ 0 };

	void UpdateBiomeColors();

	UPROPERTY()
		AExponentialHeightFog* ExponentialHeighFog;

	void FindExponentialHeightFog();

	UPROPERTY()
		ASnakeGameSnake* SnakeVisual;

	uint32 SnakeColorTableIndex{ 0 };

	void UpdateSnakeColors();

private:

	UFUNCTION(Exec, Category = "Cheat Console Command")
		void NextBiome();
};

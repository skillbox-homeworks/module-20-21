// SnakeGame, Copyright FaNtic. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Core/Types.h"
#include "SnakeGamePawn.generated.h"

class UCameraComponent;

UCLASS()
class SNAKEGAME_API ASnakeGamePawn : public APawn
{
	GENERATED_BODY()

public:

	ASnakeGamePawn();

protected:

	UPROPERTY(VisibleAnywhere)
		USceneComponent* Origin;

	UPROPERTY(VisibleAnywhere)
		UCameraComponent* Camera;

	UPROPERTY(EditDefaultsOnly, meta = (Category = "Custom Camera Settings", ClampMin = "1", ClampMax = "2"))
		float CameraZoom{ 1.0f };

public:

	void UpdateCameraLocation(const SnakeGame::Dimension& InitialDimension, uint32 InitialCellSize, const FTransform InitialGridTransform);

private:

	SnakeGame::Dimension Dimension;
	uint32 CellSize;
	FTransform GridTransform;

	FDelegateHandle ViewportResizedHandle;

	void OnViewportResized(FViewport* Viewport, uint32 Value);

};

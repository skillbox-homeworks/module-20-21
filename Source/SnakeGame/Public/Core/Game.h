// SnakeGame, Copyright FaNtic. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"

namespace SnakeGame
{
	class Grid;
	class Snake;

	class Game
	{
	public:

		Game(const Settings& Settings);

	public:

		TSharedPtr<Grid> GetGrid() const { return GameGrid; }

		TSharedPtr<Snake> GetSnake() const { return GameSnake; }

		void Update(float DeltaSeconds, const Input& SnakeInput);

	private:

		const Settings GameSettings;

		TSharedPtr<Grid> GameGrid;
		TSharedPtr<Snake> GameSnake;

		float PastSeconds{ 0.0f };

		bool UpdateTime(float DeltaSeconds);

		bool GameOver = false;

		bool SnakeIsAlive() const;

		void UpdateGrid();
		void MoveSnake(const Input& SnakeInput);
	};
}



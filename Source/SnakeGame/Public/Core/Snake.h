// SnakeGame, Copyright FaNtic. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"

namespace SnakeGame
{
	class Snake
	{

	public:

		Snake(const SnakeSettings& Settings);

	public:

		const TSnakeList& GetSnakeLinks() const { return SnakeLinks; }
		const FIntPoint GetSnakeHead() const { return SnakeLinks.GetHead()->GetValue(); }
		TSnakeListNode* GetSnakeBodyStartNode() const { return SnakeLinks.GetHead()->GetNextNode(); }

	private:

		TSnakeList SnakeLinks;

	public:

		void Move(const Input& SnakeInput);

	private:

		Input LastSnakeInput{ 1, 0 };
	};
}

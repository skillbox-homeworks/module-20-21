// SnakeGame, Copyright FaNtic. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Containers/List.h"

namespace SnakeGame
{

	struct Dimension
	{
		uint32 Width;
		uint32 Height;
	};

	enum class CellType
	{
		Empty = 0,
		Wall,
		Snake
		//Food
	};

	struct Input
	{
		int8 X;
		int8 Y;

		inline bool OppositeDirection(const Input& NewInput) const
		{
			return (X == -NewInput.X && X != 0) || (Y == -NewInput.Y && Y != 0);
		}
	};

	struct SnakeSettings
	{
		uint32 DefaultSnakeSize{ 4 };  // Size with snake head
		FIntPoint StartPosition{ 0, 0 };
		float Speed{ 1.0f };
	};

	struct Settings
	{
		Dimension GridSize{ 10, 10 };
		SnakeSettings Snake;
	};

	using TSnakeList = TDoubleLinkedList<FIntPoint>;
	using TSnakeListNode = TSnakeList::TDoubleLinkedListNode;
}
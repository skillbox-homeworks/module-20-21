// SnakeGame, Copyright FaNtic. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"

namespace SnakeGame
{
	class Grid
	{

	public:

		Grid(const Dimension& GameGridDimension);

	public:

		Dimension GetGridWithWallsDimension() const
		{
			return GridWithWallsDimension;
		}

		void UpdateGridWithSnake(TSnakeListNode* SnakeNode, CellType SnakeCellType);

		bool CheckHit(const FIntPoint& SnakeHeadPosition, CellType HitCellType) const;

	private:

		const Dimension GridWithWallsDimension;

		TArray<CellType> GridCells;

		void FreeCells(CellType CellTypeToFree);

		inline uint32 PositionToIndex(uint32 x, uint32 y) const;
		inline uint32 PositionToIndex(const FIntPoint& Point) const;

		void InitWalls();

	public:

		void PrintDebugGrid();
	};
}
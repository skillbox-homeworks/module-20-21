// SnakeGame, Copyright FaNtic. All Rights Reserved.

#include "World/SnakeGameGrid.h"
#include "Core/Grid.h"
#include "DrawDebugHelpers.h"
#include "Components/LineBatchComponent.h"
#include "Components/StaticMeshComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogWorldGrid, All, All);

ASnakeGameGrid::ASnakeGameGrid()
{
	PrimaryActorTick.bCanEverTick = false;

	Origin = CreateDefaultSubobject<USceneComponent>("Origin");
	check(Origin);
	SetRootComponent(Origin);

	GridStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("Grid Mesh");
	check(GridStaticMesh);
	GridStaticMesh->SetupAttachment(Origin);
}

void ASnakeGameGrid::BeginPlay()
{
	Super::BeginPlay();

}

void ASnakeGameGrid::SetGridModel(const TSharedPtr<SnakeGame::Grid>& Grid, uint32 InitialCellSize)
{
	if (!Grid.IsValid())
	{
		UE_LOG(LogWorldGrid, Fatal, TEXT("Grid is null, game aborted!"))
	}

	GridDimension = Grid.Get()->GetGridWithWallsDimension();
	CellSize = InitialCellSize;
	WorldWidth = GridDimension.Width * CellSize;
	WorldHeight = GridDimension.Height * CellSize;

	// Scaling StaticMeshComponent
	check(GridStaticMesh->GetStaticMesh());
	const FBox GridBox = GridStaticMesh->GetStaticMesh()->GetBoundingBox();
	const auto Size = GridBox.GetSize();

	check(Size.X);
	check(Size.Y);
	GridStaticMesh->SetRelativeScale3D(FVector(WorldHeight / Size.X, WorldWidth / Size.Y, 1.0f));
	GridStaticMesh->SetRelativeLocation(0.5f * FVector(WorldHeight, WorldWidth, -Size.Z));

	// Setting material and properties
	GridMaterial = GridStaticMesh->CreateAndSetMaterialInstanceDynamic(0);
	if (GridMaterial)
	{
		GridMaterial->SetVectorParameterValue("CellNum", FVector(GridDimension.Height, GridDimension.Width, 0.0f));
	}
}

void ASnakeGameGrid::UpdateGridColors(const FBiomeColors& Colors)
{
	if (GridMaterial)
	{
		GridMaterial->SetVectorParameterValue("BackgroundColor", Colors.BackgroundColor);
		GridMaterial->SetVectorParameterValue("LineColor", Colors.LineColor);
		GridMaterial->SetVectorParameterValue("WallColor", Colors.WallColor);
	}
}

void ASnakeGameGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//DrawGrid();
}

void ASnakeGameGrid::DrawGrid()
{
	if (!GetWorld() || !GetWorld()->LineBatcher)
	{
		return;
	}

	for (uint32 i = 0; i < GridDimension.Height + 1; ++i)
	{
		const FVector StartLocation = GetActorLocation() + GetActorForwardVector() * CellSize * i;
		DrawDebugLine(GetWorld(), StartLocation, StartLocation + GetActorRightVector() * WorldWidth, FColor::Red, false, -1, 0, 1.0f);
		//GetWorld()->LineBatcher->DrawLine(StartLocation, StartLocation + GetActorRightVector() * WorldWidth, FLinearColor::Red, 1, 2.0f);
	}

	for (uint32 i = 0; i < GridDimension.Width + 1; ++i)
	{
		const FVector StartLocation = GetActorLocation() + GetActorRightVector() * CellSize * i;
		DrawDebugLine(GetWorld(), StartLocation, StartLocation + GetActorForwardVector() * WorldHeight, FColor::Red, false, -1, 0, 1.0f);
		//GetWorld()->LineBatcher->DrawLine(StartLocation, StartLocation + GetActorForwardVector() * WorldHeight, FLinearColor::Red, 1, 2.0f);
	}

}


// SnakeGame, Copyright FaNtic. All Rights Reserved.

#include "World/SnakeGameSnake.h"
#include "World/SnakeGameSnakeElementBase.h"

namespace
{
	FVector NodePositionToVector(const FIntPoint& Position, uint32 CellSize, const SnakeGame::Dimension& Dimension)
	{
		return FVector{ (Dimension.Height - 1 - static_cast<float>(Position.Y)) * CellSize, static_cast<float>(Position.X) * CellSize, 0.0f } + FVector(CellSize * 0.5);
	}
}

ASnakeGameSnake::ASnakeGameSnake()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASnakeGameSnake::BeginPlay()
{
	Super::BeginPlay();

	if (!Snake.IsValid() || !GetWorld()) return;

	const auto& Links = Snake.Pin()->GetSnakeLinks();

	uint32 i = 0;
	for (const auto& Link : Links)
	{
		const bool IsHead = i == 0;
		const FTransform NodeTransform = FTransform(NodePositionToVector(Link, CellSize, Dimension));
		auto* LinkActor = GetWorld()->SpawnActorDeferred<ASnakeGameSnakeElementBase>(IsHead ? SnakeHeadClass : SnakeBodyClass, NodeTransform);
		LinkActor->UpdateScale(CellSize);
		LinkActor->FinishSpawning(NodeTransform);
		SnakeNodes.Add(LinkActor);
		++i;
	}
}

void ASnakeGameSnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!Snake.IsValid() || !GetWorld()) return;

	const auto& Nodes = Snake.Pin()->GetSnakeLinks();
	auto* NodePtr = Nodes.GetHead();

	for (auto* NodeActor : SnakeNodes)
	{
		NodeActor->SetActorLocation(NodePositionToVector(NodePtr->GetValue(), CellSize, Dimension));
		NodePtr = NodePtr->GetNextNode();
	}
}

void ASnakeGameSnake::SetModel(const TSharedPtr<SnakeGame::Snake>& InitialSnake, uint32 InitialCellSize, const SnakeGame::Dimension& InitialDimension)
{
	Snake = InitialSnake;
	CellSize = InitialCellSize;
	Dimension = InitialDimension;
}

void ASnakeGameSnake::UpdateSnakeColors(const FSnakeColors& Colors)
{
	for (int32 i = 0; i < SnakeNodes.Num(); ++i)
	{
		SnakeNodes[i]->UpdateColor(i == 0 ? Colors.HeadColor : Colors.BodyColor);
	}
}


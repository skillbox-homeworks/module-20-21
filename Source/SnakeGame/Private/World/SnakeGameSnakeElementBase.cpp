// SnakeGame, Copyright FaNtic. All Rights Reserved.

#include "World/SnakeGameSnakeElementBase.h"
#include "Components/StaticMeshComponent.h"

ASnakeGameSnakeElementBase::ASnakeGameSnakeElementBase()
{
	PrimaryActorTick.bCanEverTick = false;

	Origin = CreateDefaultSubobject<USceneComponent>("Origin");
	check(Origin);
	SetRootComponent(Origin);

	ElementStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Snake Element Mesh");
	check(ElementStaticMeshComponent);
	ElementStaticMeshComponent->SetupAttachment(Origin);
}

void ASnakeGameSnakeElementBase::UpdateColor(const FLinearColor& Color)
{
	auto* ElementMaterial = ElementStaticMeshComponent->CreateAndSetMaterialInstanceDynamic(0);
	if (ElementMaterial)
	{
		ElementMaterial->SetVectorParameterValue("Color", Color);
	}
}

void ASnakeGameSnakeElementBase::UpdateScale(uint32 CellSize)
{
	check(ElementStaticMeshComponent->GetStaticMesh());
	const FBox Box = ElementStaticMeshComponent->GetStaticMesh()->GetBoundingBox();
	const auto Size = Box.GetSize();

	check(Size.X);
	check(Size.Y);
	check(Size.Z);
	ElementStaticMeshComponent->SetRelativeScale3D(FVector(CellSize / Size.X, CellSize / Size.Y, CellSize / Size.Z));
}

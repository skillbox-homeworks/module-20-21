// SnakeGame, Copyright FaNtic. All Rights Reserved.

#include "Core/Game.h"
#include "Core/Grid.h"
#include "Core/Snake.h"

DEFINE_LOG_CATEGORY_STATIC(LogGame, All, All);

using namespace SnakeGame;

Game::Game(const Settings& Settings) : GameSettings(Settings)
{
	GameGrid = MakeShared<Grid>(Settings.GridSize);
	GameSnake = MakeShared<Snake>(Settings.Snake);

	UpdateGrid();
}

void Game::UpdateGrid()
{
	GameGrid->UpdateGridWithSnake(GameSnake->GetSnakeBodyStartNode(), CellType::Snake);
	GameGrid->PrintDebugGrid();
}

void Game::Update(float DeltaSeconds, const Input& SnakeInput)
{
	if (GameOver || !UpdateTime(DeltaSeconds)) return;
	MoveSnake(SnakeInput);

	if (!SnakeIsAlive())
	{
		GameOver = true;
		UE_LOG(LogGame, Display, TEXT("------ GAME OVER ------"));
	}
}

bool Game::UpdateTime(float DeltaSeconds)
{
	PastSeconds += DeltaSeconds;
	if (PastSeconds < GameSettings.Snake.Speed) return false;

	PastSeconds = 0.0f;
	return true;
}

void Game::MoveSnake(const Input& SnakeInput)
{
	GameSnake->Move(SnakeInput);
	UpdateGrid();
}

bool Game::SnakeIsAlive() const
{
	return !(GameGrid->CheckHit(GameSnake->GetSnakeHead(), CellType::Wall) || GameGrid->CheckHit(GameSnake->GetSnakeHead(), CellType::Snake));
}

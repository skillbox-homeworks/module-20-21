// SnakeGame, Copyright FaNtic. All Rights Reserved.

#include "Core/Grid.h"

DEFINE_LOG_CATEGORY_STATIC(LogGrid, All, All);

using namespace SnakeGame;

Grid::Grid(const Dimension& GameGridDimension) : GridWithWallsDimension(Dimension{ GameGridDimension.Width + 2, GameGridDimension.Height + 2 })
{
	GridCells.Init(CellType::Empty, GridWithWallsDimension.Width * GridWithWallsDimension.Height);

	InitWalls();
	PrintDebugGrid();
}

void SnakeGame::Grid::FreeCells(CellType CellTypeToFree)
{
	for (auto& Cell : GridCells)
	{
		if (Cell == CellTypeToFree)
		{
			Cell = CellType::Empty;
		}
	}
}

bool Grid::CheckHit(const FIntPoint& SnakeHeadPosition, CellType HitCellType) const
{
	return GridCells[PositionToIndex(SnakeHeadPosition)] == HitCellType;
}

void SnakeGame::Grid::UpdateGridWithSnake(TSnakeListNode* SnakeNode, CellType SnakeCellType)
{
	FreeCells(SnakeCellType);

	TSnakeListNode* Node = SnakeNode;
	while (Node)
	{
		const auto index = PositionToIndex(Node->GetValue());
		GridCells[index] = SnakeCellType;
		Node = Node->GetNextNode();
	}
}


inline uint32 Grid::PositionToIndex(uint32 x, uint32 y) const
{
	return y * GridWithWallsDimension.Width + x;
}

inline uint32 Grid::PositionToIndex(const FIntPoint& Point) const
{
	return PositionToIndex(static_cast<uint32>(Point.X), static_cast<uint32>(Point.Y));
}


void Grid::InitWalls()
{
	for (uint32 y = 0; y < GridWithWallsDimension.Height; ++y)
	{
		for (uint32 x = 0; x < GridWithWallsDimension.Width; ++x)
		{
			if (y == 0 || x == 0 || x == GridWithWallsDimension.Width - 1 || y == GridWithWallsDimension.Height - 1)
			{
				GridCells[PositionToIndex(x, y)] = CellType::Wall;
			}
		}
	}
}

void Grid::PrintDebugGrid()
{
#if !UE_BUILD_SHIPPING

	for (uint32 y = 0; y < GridWithWallsDimension.Height; ++y)
	{
		FString Line;

		for (uint32 x = 0; x < GridWithWallsDimension.Width; ++x)
		{
			TCHAR Symbol{};  // ������������� ������, ����� ������ �������

			switch (GridCells[PositionToIndex(x, y)])
			{
			case CellType::Empty: Symbol = '0'; break;
			case CellType::Wall: Symbol = '*'; break;
			case CellType::Snake: Symbol = 's'; break;
			}

			Line.AppendChar(Symbol).AppendChar(' ');
		}
		UE_LOG(LogGrid, Display, TEXT("% s"), *Line);
	}

#endif
}


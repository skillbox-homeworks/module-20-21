// SnakeGame, Copyright FaNtic. All Rights Reserved.

#include "Core/Snake.h"

using namespace SnakeGame;

Snake::Snake(const SnakeSettings& Settings)
{
	checkf(Settings.DefaultSnakeSize >= 4, TEXT("Default snake size is too small: %i"), Settings.DefaultSnakeSize);

	const FIntPoint StartPosition = Settings.StartPosition;
	for (uint32 i = 0; i < Settings.DefaultSnakeSize; ++i)
	{
		SnakeLinks.AddTail(FIntPoint{ StartPosition.X - static_cast<int32>(i), StartPosition.Y });
	}
}

void Snake::Move(const Input& SnakeInput)
{
	// Register inputs only if SnakeInput is not opposite to self
	if (!LastSnakeInput.OppositeDirection(SnakeInput))
	{
		LastSnakeInput = SnakeInput;
	}

	SnakeLinks.RemoveNode(SnakeLinks.GetTail());
	SnakeLinks.InsertNode(SnakeLinks.GetHead()->GetValue(), SnakeLinks.GetHead()->GetNextNode());
	SnakeLinks.GetHead()->GetValue() += FIntPoint(LastSnakeInput.X, LastSnakeInput.Y);
}

// SnakeGame, Copyright FaNtic. All Rights Reserved.

#include "Framework/SnakeGamePawn.h"
#include "Camera/CameraComponent.h"

namespace
{
	float GetRadHalfHFOV(UCameraComponent* Camera)
	{
		return FMath::DegreesToRadians(Camera->FieldOfView * 0.5);
	}
	float GetRadHalfVFOV(UCameraComponent* Camera, float VieportAspectRatio)
	{
		return FMath::Atan(FMath::Tan(FMath::DegreesToRadians(0.5f * Camera->FieldOfView)) * 1 / VieportAspectRatio);
	}
}

ASnakeGamePawn::ASnakeGamePawn()
{
	PrimaryActorTick.bCanEverTick = false;

	Origin = CreateDefaultSubobject<USceneComponent>("Origin");
	check(Origin);
	SetRootComponent(Origin);

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	check(Camera);
	Camera->SetupAttachment(Origin);
}

void ASnakeGamePawn::UpdateCameraLocation(const SnakeGame::Dimension& InitialDimension, uint32 InitialCellSize, const FTransform InitialGridTransform)
{
	Dimension = InitialDimension;
	CellSize = InitialCellSize;
	GridTransform = InitialGridTransform;

	check(GEngine);
	check(GEngine->GameViewport);
	check(GEngine->GameViewport->Viewport);

	auto* Viewport = GEngine->GameViewport->Viewport;
	Viewport->ViewportResizedEvent.Remove(ViewportResizedHandle);
	ViewportResizedHandle = Viewport->ViewportResizedEvent.AddUObject(this, &ASnakeGamePawn::OnViewportResized);

#if WITH_EDITOR
	OnViewportResized(Viewport, 0);
#endif
}

void ASnakeGamePawn::OnViewportResized(FViewport* Viewport, uint32 Value)
{
	if (!Viewport || Viewport->GetSizeXY().Y == 0 || Dimension.Height == 0)
	{
		return;
	}

	const float WorldWidth = Dimension.Width * CellSize;
	const float WorldHeight = Dimension.Height * CellSize;

	const float ViewportAspectRatio = static_cast<float>(Viewport->GetSizeXY().X) / Viewport->GetSizeXY().Y;
	const float GridAspectRatio = static_cast<float>(Dimension.Width) / Dimension.Height;

	check(ViewportAspectRatio);

	float LocationZ = 0.0f;

	if (ViewportAspectRatio <= GridAspectRatio)
	{
		LocationZ = 0.5f * WorldWidth / FMath::Tan(GetRadHalfHFOV(Camera));
	}
	else
	{
		LocationZ = 0.5f * WorldHeight / FMath::Tan(GetRadHalfVFOV(Camera, ViewportAspectRatio));
	}

	LocationZ = LocationZ * CameraZoom;

	const FVector NewPawnLocation = GridTransform.GetLocation() + FVector(0.5f * WorldHeight, 0.5f * WorldWidth, LocationZ);
	SetActorLocation(NewPawnLocation);
}

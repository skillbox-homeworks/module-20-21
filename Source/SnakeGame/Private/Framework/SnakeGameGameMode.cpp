// SnakeGame, Copyright FaNtic. All Rights Reserved.

#include "Framework/SnakeGameGameMode.h"
#include "Framework/SnakeGamePawn.h"
#include "Core/Types.h"
#include "Core/Grid.h"
#include "World/SnakeGameGrid.h"
#include "World/SnakeGameBiomeColors.h"
#include "World/SnakeGameSnakeColors.h"
#include "World/SnakeGameSnake.h"
#include "Engine/ExponentialHeightFog.h"
#include "Components/ExponentialHeightFogComponent.h"
#include "Kismet/GameplayStatics.h"
#include "EnhancedInput/Public/EnhancedInputSubsystems.h"
#include "EnhancedInput/Public/EnhancedInputComponent.h"

ASnakeGameGameMode::ASnakeGameGameMode()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASnakeGameGameMode::StartPlay()
{
	Super::StartPlay();

	// Init Core Game
	SnakeGame::Settings GameSettings{ GridSize.X, GridSize.Y };

	GameSettings.Snake.Speed = SnakeSpeed;
	GameSettings.GridSize = SnakeGame::Dimension{ static_cast<uint32>(GridSize.X), static_cast<uint32>(GridSize.Y) };
	GameSettings.Snake.DefaultSnakeSize = StartSnakeSize;
	GameSettings.Snake.StartPosition = FIntPoint{ GridSize.X / 2, GridSize.Y / 2 };

	CoreGame = MakeUnique<SnakeGame::Game>(GameSettings);
	check(CoreGame.IsValid());

	// Init World Grid
	const FTransform OriginTransform = FTransform::Identity;
	check(GetWorld());
	GridVisual = GetWorld()->SpawnActorDeferred<ASnakeGameGrid>(GridVisualClass, OriginTransform);
	check(GridVisual);
	GridVisual->SetGridModel(CoreGame->GetGrid(), CellSize);
	GridVisual->FinishSpawning(OriginTransform);

	// Init World Snake
	SnakeVisual = GetWorld()->SpawnActorDeferred<ASnakeGameSnake>(SnakeVisualClass, OriginTransform);
	SnakeVisual->SetModel(CoreGame->GetSnake(), CellSize, CoreGame->GetGrid()->GetGridWithWallsDimension());
	SnakeVisual->FinishSpawning(OriginTransform);

	// Init Pawn
	auto* PlayerController = GetWorld()->GetFirstPlayerController();
	check(PlayerController);

	auto* Pawn = Cast<ASnakeGamePawn>(PlayerController->GetPawn());
	check(Pawn);
	Pawn->UpdateCameraLocation(CoreGame->GetGrid()->GetGridWithWallsDimension(), CellSize, OriginTransform);

	// Update biome colors
	FindExponentialHeightFog();

	check(BiomeColorsTable);
	check(BiomeColorsTable->GetRowNames().Num() >= 1);
	BiomeColorTableIndex = FMath::RandRange(0, BiomeColorsTable->GetRowNames().Num() - 1);
	UpdateBiomeColors();

	check(SnakeColorsTable);
	check(SnakeColorsTable->GetRowNames().Num() >= 1);
	SnakeColorTableIndex = FMath::RandRange(0, SnakeColorsTable->GetRowNames().Num() - 1);
	UpdateSnakeColors();

	// Setup Enhanced Input
	SetupInput();
}

void ASnakeGameGameMode::FindExponentialHeightFog()
{
	TArray<AActor*> ExponentialHeightFogs;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AExponentialHeightFog::StaticClass(), ExponentialHeightFogs);
	if (ExponentialHeightFogs.Num() > 0)
	{
		ExponentialHeighFog = Cast<AExponentialHeightFog>(ExponentialHeightFogs[0]);
	}
}

void ASnakeGameGameMode::SetupInput()
{
	if (!GetWorld())
		return;

	if (APlayerController* PlayerController = Cast<APlayerController>(GetWorld()->GetFirstPlayerController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* InputSubSystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			InputSubSystem->ClearAllMappings();

			InputSubSystem->AddMappingContext(MovementInputMapping.LoadSynchronous(), 0);
		}

		UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerController->InputComponent);
		check(EnhancedInputComponent);

		EnhancedInputComponent->BindAction(MoveForwardInputAction.LoadSynchronous(), ETriggerEvent::Started, this, &ThisClass::OnMoveForward);
		EnhancedInputComponent->BindAction(MoveRightInputAction.LoadSynchronous(), ETriggerEvent::Started, this, &ThisClass::OnMoveRight);
	}
}

void ASnakeGameGameMode::OnMoveForward(const FInputActionValue& Value)
{
	const float InputValue = Value.Get<float>();

	if (InputValue == 0)
		return;

	SnakeInput = SnakeGame::Input{ 0, static_cast<int8>(InputValue) };
}

void ASnakeGameGameMode::OnMoveRight(const FInputActionValue& Value)
{
	const float InputValue = Value.Get<float>();

	if (InputValue == 0)
		return;

	SnakeInput = SnakeGame::Input{ static_cast<int8>(InputValue), 0 };
}

void ASnakeGameGameMode::UpdateSnakeColors()
{
	const FName RowName = SnakeColorsTable->GetRowNames()[SnakeColorTableIndex];
	const auto* ColorSet = SnakeColorsTable->FindRow<FSnakeColors>(RowName, {});
	if (ColorSet)
	{
		// Update head and body colors
		SnakeVisual->UpdateSnakeColors(*ColorSet);
	}
}


void ASnakeGameGameMode::UpdateBiomeColors()
{
	const FName RowName = BiomeColorsTable->GetRowNames()[BiomeColorTableIndex];
	const auto* ColorSet = BiomeColorsTable->FindRow<FBiomeColors>(RowName, {});
	if (ColorSet)
	{
		// Update background, line and wall colors
		GridVisual->UpdateGridColors(*ColorSet);

		// Update fog color
		if (ExponentialHeighFog && ExponentialHeighFog->GetComponent())
		{
			ExponentialHeighFog->GetComponent()->FogInscatteringColor = ColorSet->SkyAtmosphereColor;
			ExponentialHeighFog->MarkComponentsRenderStateDirty();
		}
	}
}

void ASnakeGameGameMode::NextBiome()
{
	if (BiomeColorsTable)
	{
		BiomeColorTableIndex = (BiomeColorTableIndex + 1) % BiomeColorsTable->GetRowNames().Num();
		UpdateBiomeColors();
	}
}

void ASnakeGameGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CoreGame.IsValid())
	{
		CoreGame->Update(DeltaSeconds, SnakeInput);
	}
}
